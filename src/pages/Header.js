import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import notificationsIcon from "../assets/images/icon-doorbell.png";
import user from "../assets/images/icon-user.png";
import "./styles/header.css";

function Notification(props) {
  const { title, date } = props;
  return (
    <div className="notifications-preview-content">
      <h1>{title}</h1>
      <p>{date}</p>
    </div>
  );
}

export default function Header() {
  const [name, setName] = useState();
  const [IC, setIC] = useState();
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [notifications, setNotifications] = useState([]);
  const el = [
    <Notification
      title={`Possible tsunami in Bayan Lepas`}
      date={"18/01/2050"}
    />,
    <Notification
      title={`Formation of tsunami occurred in Bayan Lepas `}
      date={"22/01/2050"}
    />,
    <Notification
      title={`Possible shortage of water at the Nigeria`}
      date={"27/05/2050"}
    />,
    <Notification title={`Shortage of food in Nigeria`} date={"09/06/2050"} />,
    <Notification
      title={`Formation of acid rain at the South East Penang`}
      date={"18/06/2050"}
    />
  ];
  let index = 1;
  useEffect(_ => {
    let interval = setInterval(_ => {
      if (index <= el.length) {
        setNotifications([el.slice(0, index)]);
        index++;
      } else {
        return _ => clearInterval(interval);
      }
    }, 1000);
    // eslint-disable-next-line
  }, []);
  function handleClick() {
    let elem = document.getElementById("root");
    let elemStyle = getComputedStyle(elem);
    if (elemStyle.getPropertyValue("background-size") !== "cover")
      elem.style.backgroundSize = "cover";
  }
  function handleOnClick() {
    let elem = document.getElementById("root");
    elem.style.backgroundSize = 0;
  }
  function handleBtnClick() {
    if (name === "Melwin" && IC === "012345-07-8910") {
      setIsLoggedIn(true);
    } else {
      alert("Please input the correct details!");
    }
  }

  return (
    <header>
      <ul>
        <li>
          <Link to="/" onClick={handleClick}>
            Home
          </Link>
        </li>
        <li>
          <Link to="/food_program" onClick={handleClick}>
            Food Program
          </Link>
        </li>
        <li>
          <Link to="/water_program" onClick={handleClick}>
            Water Program
          </Link>
        </li>
        <li>
          <Link to="/weather_forecast" onClick={handleClick}>
            Weather Forecast
          </Link>
        </li>
        <li>
          <Link to="/water_pollution" onClick={handleClick}>
            Water Pollution
          </Link>
        </li>
      </ul>
      <div className="right">
        <img src={notificationsIcon} alt="Bell icon" />
        <img src={user} alt="User icon" />
        <div className="notifications-preview">
          <h1>NOTIFICATIONS</h1>
          <div className="notifications-preview-container">{notifications}</div>
          <Link to="/notifications" className="view" onClick={handleOnClick}>
            View All
          </Link>
        </div>
        {isLoggedIn ? (
          <div className="login-form">
            <p className="welcome-text">Welcome {name}!</p>
          </div>
        ) : (
          <form className="login-form">
            <label htmlFor="name">NAME</label>
            <input
              id="name"
              value={name}
              onChange={e => setName(e.target.value)}
            />
            <label htmlFor="ic">IC</label>
            <input id="ic" value={IC} onChange={e => setIC(e.target.value)} />
            <div className="login" onClick={handleBtnClick}>
              <p>LOGIN</p>
            </div>
          </form>
        )}
      </div>
    </header>
  );
}
