import React from "react";
import "./styles/waterprogram.css";
import pin_red from "../assets/images/SVG/pin-red.svg";
import pin_yellow from "../assets/images/SVG/pin-yellow.svg";
import pin_green from "../assets/images/SVG/pin-green.svg";
import "./styles/pin.css";

export default function Pin(props) {
  const { color, country, onClick } = props;
  let imgSrc;
  switch (color) {
    case "red":
      imgSrc = pin_red;
      break;
    case "yellow":
      imgSrc = pin_yellow;
      break;
    case "green":
      imgSrc = pin_green;
      break;
    default:
      imgSrc = null;
      break;
  }

  return (
    <img
      src={imgSrc}
      draggable={false}
      className={`pin ${color} ${country}`}
      alt={`${color}-pin`}
      onClick={_ => onClick(country)}
    />
  );
}
