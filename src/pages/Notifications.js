import React, { useEffect } from "react";
import { Route, Link, withRouter } from "react-router-dom";
import "./styles/notifications.css";

function LeftBar(props) {
  const { content } = props;
  return (
    <Link
      className="selection"
      to={`/notifications/${content.toLowerCase().replace(" ", "_")}`}
    >
      <p>{content}</p>
    </Link>
  );
}

function RightContent(props) {
  const { date, titles, contents } = props;
  return (
    <div className="panel">
      <p className="date">{date}</p>
      {contents.map((con, i) => (
        <div className="notifications-content-container">
          <h1 className="notifications-title">{titles[i]}</h1>
          <p className="notifications-content">{con}</p>
        </div>
      ))}
    </div>
  );
}

function Food() {
  return (
    <div className="right-display">
      <div className="arrow"></div>

      <RightContent
        date="09/06/2050"
        titles={["Shortage of food in Nigeria"]}
        contents={[
          "Shortage of food in Nigeria. It is advised for other countries to immediately provide aid to Nigeria to prevent famines. If critical shortage of food in Nigeria occurs, immediately transfer of food needed."
        ]}
      />
    </div>
  );
}
function Water() {
  return (
    <div className="right-display">
      <div className="arrow"></div>

      <RightContent
        date="12/11/2019"
        titles={["Possible shortage of water at the Nigeria"]}
        contents={[
          "Possible shortage of water at the Nigeria. It is advised for other countries to immediately provide aid to Nigeria to prevent water scarcity."
        ]}
      />
    </div>
  );
}
function Weather() {
  return (
    <div className="right-display">
      <div className="arrow"></div>

      <RightContent
        date="18/01/2050"
        titles={["Possible tsunami in Bayan Lepas"]}
        contents={[
          "Possible tsunami in Bayan Lepas. Citizens are advised to evacuate to the nearest safe zone to avoid the tsunami"
        ]}
      />
      <RightContent
        date="22/01/2050"
        titles={["Formation of tsunami occurred in Bayan Lepas"]}
        contents={[
          "Formation of tsunami occurred in Bayan Lepas. Citizens are advised to evacuate to the nearest safe zone to avoid the tsunami"
        ]}
      />
    </div>
  );
}
function WaterPollution() {
  return (
    <div className="right-display">
      <div className="arrow"></div>
      <RightContent
        date="18/06/2050"
        titles={["Formation of acid rain at the South East Penang"]}
        contents={[
          "Possible acid rain at the South East Penang. pH of nearby rivers severely affected. early reports indicate pH of nearby rivers has dropped to 2.1 immediate action required to neutralise the acidity"
        ]}
      />
    </div>
  );
}

function Notifications(props) {
  const { location } = props;
  useEffect(_ => {
    let elem = document.getElementById("root");
    let elemStyle = getComputedStyle(elem);
    if (elemStyle.getPropertyValue("background-size") === "cover")
      elem.style.backgroundSize = 0;
  }, []);
  useEffect(
    _ => {
      let index;
      let arrow = document.getElementsByClassName("arrow")[0];
      let path = location.pathname.split("/")[2];
      switch (path) {
        case "food":
          index = 4;
          break;
        case "water":
          index = 3;
          break;
        case "weather":
          index = 2;
          break;
        case "water_pollution":
          index = 1;
          break;
        default:
          break;
      }
      if (arrow) {
        arrow.style.top = `calc(100vh - (49vh / 5 * ${index}) - ${index}0vh)`;
        arrow.style.opacity = 1;
      }
    },
    [location]
  );

  return (
    <div className="notifications">
      <div className="left-selection">
        <LeftBar content="Food" />
        <LeftBar content="Water" />
        <LeftBar content="Weather" />
        <LeftBar content="Water Pollution" />
      </div>
      <Route path="/notifications/food">
        <Food />
      </Route>
      <Route path="/notifications/water">
        <Water />
      </Route>
      <Route path="/notifications/weather">
        <Weather />
      </Route>
      <Route path="/notifications/water_pollution">
        <WaterPollution />
      </Route>
    </div>
  );
}
export default withRouter(Notifications);
